from django import forms

from .models import Community


class NewCommunityForm(forms.ModelForm):
    class Meta:
        model = Community
        fields = ['name']
