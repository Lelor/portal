from django.apps import apps
from django.shortcuts import get_object_or_404
from django.http import HttpResponseBadRequest
from community import models, forms

PUBLICATION_MODEL = apps.get_model('publication', 'Publication')


def get_member_communities(user):
    communities = models.CommunityMember.objects.filter(user=user)
    return (x.community for x in communities)


def get_user_communities(user):
    return models.Community.objects.filter(admin=user)


def get_community_publications(comm_id):
    """"""
    comm = get_object_or_404(models.Community, pk=comm_id)
    return PUBLICATION_MODEL.objects.filter(community=comm)


def register_community(request):
    """Cadastro de nova comunidade."""
    form = forms.NewCommunityForm(request.POST)
    if form.is_valid():
        comm = form.save(commit=False)
        comm.admin = request.user
        return form.save()
    return HttpResponseBadRequest
