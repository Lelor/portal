from django.urls import path

from .views import views, register

urlpatterns = [
    path('my_communities', views.user_communities, name='user_communities'),
    path('<int:comm_id>', views.community_screen, name='community'),
    path('new_community', register.register_new_community, name='new_community')
]
