from django.urls import path

from .views import profile

urlpatterns = [
    path('user/update', profile.update_user, name='update_user'),
    path('user/create', profile.create_user, name='create_user')
]
