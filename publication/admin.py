from django.contrib import admin

from .models import Comment, Publication, PublicationType, Tag

admin.site.register(Publication)
admin.site.register(PublicationType)
admin.site.register(Comment)
admin.site.register(Tag)
