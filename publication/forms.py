"""Forms relevates ao app de publicação"""
from django import forms

from .models import Comment, CommentLike


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['comment_text']


class EvaluationForm(forms.ModelForm):
    class Meta:
        model = CommentLike
        fields = ['positive']
