"""Modelo de perfil dos usuários"""

from django.contrib.auth.models import AbstractUser
from django.db import models


class UserProfile(AbstractUser):
    """Tabela de usuários."""
    birthdate = models.DateField("Data de nascimento", default=None, blank=True, null=True)

    def __str__(self):
        return self.email
