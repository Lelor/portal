from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseForbidden, HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.utils import timezone
from publication.forms import CommentForm
from publication.models import Publication, Comment
from publication.model_functions import register_comment_evaluation


@login_required
def new_comment(request, publication_id):
    """
    Rota para adicionar novo comentário.

    Args:
        request:
            Contexto da requisição recebida na rota.
        publication_id:
            ID da publicação que o comentário será feito.

    Returns:
        redirecionamento para a página da publicação.
    """
    pub = get_object_or_404(Publication, pk=publication_id)

    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.publication = pub
            comment.author = request.user
            comment.pub_date = timezone.now()
            comment.save()
            return redirect('publication', publication_id=publication_id)
        return HttpResponse(status=500)
    return HttpResponseForbidden()


@login_required
def edit_comment(request, comment_id):
    """
    Rota de edição de comentário.

    Args:
        request:
            Contexto da requisição recebida na rota.
        comment_id:
            ID do comentário.

    Returns:
        JSON de resposta caso a requisição seja um sucesso
    """
    comment = get_object_or_404(Comment, pk=comment_id)
    if request.method == 'POST':
        if request.user == comment.author:
            comment.comment_text = request.POST['comment_text']
            comment.save()
            return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    return HttpResponseForbidden()


@login_required
def evaluate_comment(request, comment_id):
    """
    Rota de likes e dislikes dos comentários.

    Args:
        request:
            Contexto da requisição recebida na rota.
        comment_id:
            ID do comentário.

    Returns:
        Código referente ao resultado da requisição.
    """
    comment = get_object_or_404(Comment, pk=comment_id)
    if request.method == 'POST':
        register_comment_evaluation(comment, request)
        return HttpResponse(status=200)
    return HttpResponseForbidden()
