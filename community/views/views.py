from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from . import model_functions


@login_required
def user_communities(request):
    context = {
        'communities': model_functions.get_member_communities(request.user),
        'admin': model_functions.get_user_communities(request.user)
    }
    return render(request, "community/user_communities.html", context=context)


def community_screen(request, comm_id):
    context = {
        'publications': model_functions.get_community_publications(comm_id)
    }
    return render(request, 'community/community.html', context=context)
