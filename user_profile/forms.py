from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils.translation import ugettext_lazy as _

from .models import UserProfile


class CustomUserCreationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(CustomUserCreationForm, self).__init__(*args, **kwargs)
        self.fields['email'].label = "Email"
        self.fields['first_name'].label = "Primeiro nome"
        self.fields['last_name'].label = "Sobrenome"
        self.fields['username'].label = "Nome de usuário"
        self.fields['username'].help_text = "Deve conter no máximo 150 caracteres"
        self.fields['password1'].label = "Senha"
        self.fields['password1'].help_text = "Deve conter no mínimo 8 caracteres"
        self.fields['password2'].label = "Confirmação da senha"
        self.fields['password2'].help_text = "Insira a mesma senha digitada acima"

    error_messages = {
        'password_mismatch': _("As senhas devem ser iguais!"),
    }

    class Meta:
        model = UserProfile
        fields = ['first_name', 'last_name', 'email', 'username']


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = UserProfile
        fields = ['first_name', 'last_name', 'email', 'birthdate']
