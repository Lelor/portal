from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from community.forms import NewCommunityForm

from .model_functions import register_community


@login_required
def register_new_community(request):
    if request.method == 'POST':
        community = register_community(request)
        return redirect('community', community.id)
    return render(request,
                  'community/new_community.html',
                  context={"community_form": NewCommunityForm})
