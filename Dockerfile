# pra subir o container do zero esteja na pasta do projeto e rode os comandos:
# docker build . -t portal
# docker run -p 5432:5432 --name="portal" -v local_db:/var/lib/postgresql/data portal

FROM postgres:latest

RUN apt-get update -y && apt-get upgrade -y && apt-get install -y vim

ENV POSTGRES_USER portal
ENV POSTGRES_PASSWORD t3st@z
ENV POSTGRES_DB portal_db

USER postgres

RUN initdb && echo "host all all 0.0.0.0/0 trust" >> /var/lib/postgresql/data/pg-hba.conf
