from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import UserProfile
from .forms import CustomUserCreationForm


class UserProfileAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    # form = CustomUserChangeForm
    model = UserProfile
    # list_display = ['email', 'username']


admin.site.register(UserProfile, UserProfileAdmin)
