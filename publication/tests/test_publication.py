from django.apps import apps
from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from publication import models


USER_MODEL = apps.get_model('user_profile', 'UserProfile')
COMMUNITY_MODEL = apps.get_model('community', 'Community')


class PublicationTests(TestCase):
    """Testes de sistema das rotas de publicação."""
    publication_data = {
        'title': 'testpub',
        'pub_date': timezone.now(),
        'text': 'test publication',
    }

    def setUp(self):
        self.user = USER_MODEL.objects.create_user(username='testuser', password='secret')
        self.community = COMMUNITY_MODEL.objects.create(name='testarea', admin=self.user)
        self.pub_type = models.PublicationType.objects.create(text='test')
        self.publication_data['user'] = self.user
        self.publication_data['pub_type'] = self.pub_type
        self.publication_data['community'] = self.community
        self.pub = models.Publication.objects.create(**self.publication_data)

    def test_publication(self):
        response = self.client.get(reverse('publication', args=(self.pub.pk,)))
        self.assertEqual(response.status_code, 200)

    def test_publication_returns_404_at_invalid_index(self):
        response = self.client.get(reverse('publication', args=(self.pub.pk+1,)))
        self.assertEqual(response.status_code, 404)

    def test_publication_renders_right_template(self):
        response = self.client.get(reverse('publication', args=(self.pub.pk,)))
        self.assertTemplateUsed(response, 'publication/publication.html')
