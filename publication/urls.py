from django.urls import path

from .views import comment, publication

urlpatterns = [
    path('all', publication.publication_list, name='publication_list'),
    path('<int:publication_id>', publication.publication, name='publication'),
    path('comments/new_comment/<int:publication_id>', comment.new_comment, name='new_comment'),
    path('comments/edit_comment/<int:comment_id>', comment.edit_comment, name='edit_comment'),
    path('comments/evaluate_comment/<int:comment_id>', comment.evaluate_comment, name='evaluate_comment'),
]
