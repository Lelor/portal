from django.apps import apps
from django.test import TestCase
from django.urls import reverse
from community.models import Community, CommunityMember


USER_MODEL = apps.get_model('user_profile', 'UserProfile')
USER_CREDENTIALS = {
    'username': 'testuser',
    'password': 'secret'
}


class TestCommunity(TestCase):
    """Testes de sistema das comunidades."""
    COMMUNITY_NAME = "Foo"
    def setUp(self):
        self.user = USER_MODEL.objects.create_user(**USER_CREDENTIALS)
        self.second_user = USER_MODEL.objects.create_user(username='seconduser', password='secret')
        self.community = Community.objects.create(name=self.COMMUNITY_NAME, admin=self.user)

    def test_authenticated_user_should_see_his_communities(self):
        """Validação da renderização das comunidades do usuário"""
        tag = '<li><a href={id_} class="white-text">{name}</a></li>'
        self.client.login(**USER_CREDENTIALS)
        response = self.client.get(reverse('user_communities'))
        self.assertInHTML(tag.format(id_=self.community.id, name=self.community.name),
                          response.content.decode('utf-8'))

    def test_authenticated_user_should_not_see_communities_he_is_not_a_part_of(self):
        """Validação da não renderização das comunidades que o usuário não faz parte"""
        tag = '<li><a href={id_} class="white-text">{name}</a></li>'
        new_comm = Community.objects.create(name="Bar", admin=self.second_user)
        self.client.login(**USER_CREDENTIALS)
        response = self.client.get(reverse('user_communities'))
        self.assertNotIn(tag.format(id_=new_comm.id, name=new_comm.name),
                         response.content.decode('utf-8'))

    def test_authenticated_user_should_see_community_he_is_part_of_but_not_adm(self):
        """Validação da renderização das comunidades que o usuário faz parte mas não é adm"""
        tag = '<li><a href={id_} class="white-text">{name}</a></li>'
        new_comm = Community.objects.create(name="Bar", admin=self.second_user)
        CommunityMember.objects.create(user=self.user, community=new_comm, staff=False)
        self.client.login(**USER_CREDENTIALS)
        response = self.client.get(reverse('user_communities'))
        self.assertInHTML(tag.format(id_=self.community.id, name=self.community.name),
                          response.content.decode('utf-8'))
        self.assertInHTML(tag.format(id_=new_comm.id, name=new_comm.name),
                          response.content.decode('utf-8'))
