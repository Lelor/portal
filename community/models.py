"""Modelos da aplicação."""
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Community(models.Model):
    """Tabela de áreas."""
    name = models.CharField(_("Nome"), max_length=200)
    admin = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    creation_time = models.DateTimeField(_("Data de criação da comunidade."), auto_now_add=True)

    def __str__(self):
        return self.name

    def members(self):
        """Membros da comunidade."""
        return CommunityMember.objects.filter(community=self)


class CommunityMember(models.Model):
    """Tabela de membros da comunidade."""
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    community = models.ForeignKey(Community, on_delete=models.CASCADE)
    staff = models.BooleanField(_("O usuário é membro da staff."), default=False)

    def __str__(self):
        return f'User={self.user}, Community={self.community.name}'
